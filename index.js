let http = require("http");

let port = 4000;
let directory = [
    {
        "Course": "Javascript"
    }
]

http.createServer((request, response) => {


    if (request.url == "/profile" && request.method == 'GET') {
        response.writeHead(200, { 'Content-Type': 'text/plain' });
        response.end('Welcome to your profile!');
    }
    if (request.url == "/courses" && request.method == 'GET') {
        response.writeHead(200, { 'Content-Type': 'text/plain' });
        response.end('Here is our courses availabe');
    }
    if (request.url == "/addCourses" && request.method == 'POST') {
        response.writeHead(200, { 'Content-Type': 'text/plain' });
        response.end('Add course to our resources');
    }


    if (request.url == "/updateCourse" && request.method == 'PUT') {
        response.writeHead(200, { 'Content-Type': 'text/plain' });
        response.end('Update a course to our resources');
    }
    if (request.url == "/archiveCourse" && request.method == 'DELETE') {
        response.writeHead(200, { 'Content-Type': 'text/plain' });
        response.end('Archive courses to our resources');
    }

    else {

        {
            response.writeHead(200, { 'Content-Type': 'text/plain' });
            response.end('Welcome to Booking System');
        }
    }



}).listen(port);
console.log(`Server is running at localhost: ${port}`);